# Verteilte_Systeme_und_SOA_Gruppe_1

## Name
- Sudoku with Friends

## Description
- A web application that lets you generate sudoku grids and fill them. However, others can play on the same grid simultaneously. 

## Installation
- Open the Backend folder as project in IntelliJ and start GameAPI.
- Open Sudoku.html in your browser.
- Open Sudoku.html in second browser to simulate a second player.

## Usage
- Choose a name and login.
- Choose a number from the list on the left, then click on the cell you want to place it into.
- If it's correct the number will appear.
- Other players will be listed on the right in there colour.
- Correct numbers from other players will be displayed in there colour.

## Support
- maxime.tissot@student.reutlingen-university.de

## Project status
- Known bugs:
- A win won't update correctly to all players