package hsrt.VSundSOA.backendapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main API to start the whole thing
 */
@SpringBootApplication
public class GameAPI {
    public static void main(String[] args) {
        SpringApplication.run(GameAPI.class, args);
    }

}
