package hsrt.VSundSOA.backendapi.fieldCreation;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

@CrossOrigin(origins = "*")
@RestController
public class FieldCreation {
    private int[][] solutionField = new int[9][9];
    private int[][] holedField = new int[9][9];
    private Random random = new Random();
    ArrayList<Integer> possibleFill = new ArrayList<>();

//Ich habe erst eine eigene brutforce Lösung ausprobieren wollen.
//Es ist mir aber nicht gelungen es rechtzeitig zum laufen zu bringen.
//Somit habe ich einfach einen Code aus dem Netz geholt und angepasst, damit es funktioniert.
//Die Überreste meines Versuches sind in holeField() und den auskommentierten Funktionen am Ende zu finden.

    /**
     * Creates a new Sudoku grid as well as the solution
     *
     * @return a list of two arrays of int arrays (int[][]), the first is the sudoku grid, the second its solution
     */
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/field/create")
    public ArrayList<int[][]> getNewField() {
        ArrayList<int[][]> fields = new ArrayList<>(Arrays.asList(holedField, solutionField));
        for (Integer x = 0; x < 9; x++) {
            for (Integer y = 0; y < 9; y++) {
                solutionField[x][y] = -1;
                holedField[x][y] = -1;
            }
        }
        centerInit();
        crossInit();
        cornerInit();

        holeField();
        return fields;
    }

    /**
     * Randomly generates the middle block of the grid
     */
    void centerInit() {
        for (int i = 0; i < 9; ++i) {
            Integer n = random.nextInt(9) + 1;
            if (!possibleFill.contains(n))
                possibleFill.add(n);
            else
                i--;
        }

        int k = 0;
        for (int i = 3; i < 6; ++i)
            for (int j = 3; j < 6; ++j)
                solutionField[i][j] = possibleFill.get(k++);
    }

    /**
     * Fills the upper, lower, left and right blocks of the grid
     */
    void crossInit() {
        for (int i = 3; i < 6; ++i) {
            int l = 0;
            for (int j = 3; j < 6; ++j) {
                if (i == 3) {
                    solutionField[i + 1][l] = solutionField[i][j];
                    solutionField[i + 2][l + 6] = solutionField[i][j];
                    ++l;
                } else if (i == 4) {
                    solutionField[i + 1][l] = solutionField[i][j];
                    solutionField[i - 1][l + 6] = solutionField[i][j];
                    ++l;
                } else if (i == 5) {
                    solutionField[i - 2][l] = solutionField[i][j];
                    solutionField[i - 1][l + 6] = solutionField[i][j];
                    ++l;
                }
            }
        }
        for (int j = 3; j < 6; ++j) {
            int l = 0;
            for (int i = 3; i < 6; ++i) {
                if (j == 3) {
                    solutionField[l][j + 1] = solutionField[i][j];
                    solutionField[l + 6][j + 2] = solutionField[i][j];
                    ++l;
                } else if (j == 4) {
                    solutionField[l][j + 1] = solutionField[i][j];
                    solutionField[l + 6][j - 1] = solutionField[i][j];
                    ++l;
                } else if (j == 5) {
                    solutionField[l][j - 2] = solutionField[i][j];
                    solutionField[l + 6][j - 1] = solutionField[i][j];
                    ++l;
                }
            }
        }
    }

    /**
     * Fills the corner blocks of the grid
     */
    void cornerInit() {
        for (int i = 0; i < 3; ++i) {
            int l = 0;
            for (int j = 3; j < 6; ++j) {
                if (i == 0) {
                    solutionField[i + 1][l] = solutionField[i][j];
                    solutionField[i + 2][l + 6] = solutionField[i][j];
                    ++l;
                } else if (i == 1) {
                    solutionField[i + 1][l] = solutionField[i][j];
                    solutionField[i - 1][l + 6] = solutionField[i][j];
                    ++l;
                } else if (i == 2) {
                    solutionField[i - 2][l] = solutionField[i][j];
                    solutionField[i - 1][l + 6] = solutionField[i][j];
                    ++l;
                }
            }
        }
        for (int i = 6; i < 9; ++i) {
            int l = 0;
            for (int j = 3; j < 6; ++j) {
                if (i == 6) {
                    solutionField[i + 1][l] = solutionField[i][j];
                    solutionField[i + 2][l + 6] = solutionField[i][j];
                    ++l;
                } else if (i == 7) {
                    solutionField[i + 1][l] = solutionField[i][j];
                    solutionField[i - 1][l + 6] = solutionField[i][j];
                    ++l;
                } else if (i == 8) {
                    solutionField[i - 2][l] = solutionField[i][j];
                    solutionField[i - 1][l + 6] = solutionField[i][j];
                    ++l;
                }
            }
        }
    }

    /**
     * Randomly removes some numbers in the solution to create a sudoku grid
     */
    private void holeField() {
        Boolean removed;
//        Integer stackX, stackY;
        for (Integer holes = 50; holes >= 0; holes--) {
//            removed = false;
            do {
                Integer x = random.nextInt(9);
                Integer y = random.nextInt(9);
                if (holedField[x][y] > 0) {
                    removed = true;
                } else {
                    holedField[x][y] = solutionField[x][y];
                    removed = false;
                }
//                stackX = holedField[x][y];
//                stackY = holedField[8 - x][8 - y];
//                holedField[x][y] = -1;
//                holedField[8 - x][8 - y] = -1;
//                if (!correctRemove(0).equals(1)) {
//                    removed = true;
//                } else {
//                    holedField[x][y] = stackX;
//                    holedField[8 - x][8 - y] = stackY;
//                }
            } while (removed);
        }
    }

    /*
    private boolean initialiseField(Integer row, Integer column) {
        ArrayList<Integer> possibleFill = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9));

        while (possibleFill.size() > 0) {
            solutionField[row][column] = possibleFill.get(random.nextInt(possibleFill.size()));
            if (correctPlacement(row, column)) {
                if (column.equals(8) && row.equals(8)) {
                    return true;
                }
                if (column.equals(8)) {
                    if (initialiseField(++row, 0)) {
                        return true;
                    }
                } else {
                    if (initialiseField(row, ++column)) {
                        return true;
                    }
                }
            } else {
                possibleFill.remove(possibleFill.indexOf(solutionField[row][column]));
            }
        }
        if (possibleFill.size() == 0) {
            System.out.println("possible fill == 0 for row " + row);
            for (Integer y = 0; y < 9; y++) {
                solutionField[row][y] = -1;
            }
            initialiseField(row, 0);
        }
        solutionField[row][column] = -1;
        return false;
    }

    private boolean correctPlacement(Integer row, Integer column) {
        Integer toTest = solutionField[row][column];
        for (Integer x = 0; x < 9; x++) {
            if (row == x) {
                continue;
            }
            if (toTest.equals(solutionField[x][column])) {
                return false;
            }
        }
        for (Integer y = 0; y < 9; y++) {
            if (column == y) {
                continue;
            }
            if (toTest.equals(solutionField[row][y])) {
                return false;
            }
        }
        Integer blockX = Math.floorDiv(row, 3) * 3;
        Integer blockY = Math.floorDiv(column, 3) * 3;
        for (Integer x = 0 + blockX; x < 3 + blockX; x++) {
            for (Integer y = 0 + blockY; y < 3 + blockY; y++) {
                if (row != x && column != y && toTest.equals(solutionField[x][y])) {
                    return false;
                }
            }
        }
        return true;
    }

    private Integer correctRemove(Integer foundSolutions) {
        Integer missingCells = 0;
        for (Integer x = 0; x < 9; x++) {
            for (Integer y = 0; y < 9; y++) {
                if (holedField[x][y] < 0) {
                    for (Integer tryThis = 1; tryThis <= 9; tryThis++) {
                        holedField[x][y] = tryThis;
                        if (correctPlacement(x, y)) {
                            for (Integer x2 = x; x2 < 9; x2++) {
                                for (Integer y2 = y; y2 < 9; y2++) {
                                    if (holedField[x2][y2] < 0) {
                                        missingCells++;
                                    }
                                }
                            }
                            if (missingCells == 0) {
                                return ++foundSolutions;
                            }
                            foundSolutions = correctRemove(foundSolutions);
                        }
                    }
                    holedField[x][y] = -1;
                }
            }
        }
        return foundSolutions;
    }
    */
}