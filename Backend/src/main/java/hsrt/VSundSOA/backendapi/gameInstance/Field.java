package hsrt.VSundSOA.backendapi.gameInstance;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Field {
    private int[][] startField = new int[9][9];
    private int[][] solutionField = new int[9][9];

    public int getCellsToFill() {
        int  cellsToFill = 0;
        for (int row = 0; row < 9; row++){
            for (int col = 0; col < 9; col++){
                if (startField[row][col] == -1){
                    cellsToFill++;
                }
            }
        }
        return cellsToFill;
    }

    public void field(){
        int[] sampleStart =
                {-1,-1,-1, 1,-1,-1,-1,-1, 4,
                  8,-1, 5, 3,-1, 6,-1,-1, 9,
                  2,-1,-1,-1, 9, 5,-1, 6,-1,
                  5, 3, 8,-1, 6,-1,-1,-1, 2,
                  4,-1, 2,-1,-1, 9, 8,-1,-1,
                 -1,-1,-1,-1,-1, 4,-1,-1, 3,
                 -1,-1, 3, 6, 2,-1, 4,-1, 1,
                  6, 8,-1, 4, 1,-1, 7,-1,-1,
                 -1,-1, 4, 9,-1,-1,-1,-1, 8};
        int[] sampleSolution =
                { 3, 9, 6, 1, 7, 2, 5, 8, 4,
                  8, 7, 5, 3, 4, 6, 2, 1, 9,
                  2, 4, 1, 8, 9, 5, 3, 6, 7,
                  5, 3, 8, 7, 6, 1, 9, 4, 2,
                  4, 1, 2, 5, 3, 9, 8, 7, 6,
                  9, 6, 7, 2, 8, 4, 1, 5, 3,
                  7, 5, 3, 6, 2, 8, 4, 9, 1,
                  6, 8, 9, 4, 1, 3, 7, 2, 5,
                  1, 2, 4, 9, 5, 7, 6, 3, 8};
    }
}
