package hsrt.VSundSOA.backendapi.gameInstance;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Placement {
    private String username;
    private Integer x;
    private Integer y;
    private Integer value;
}
