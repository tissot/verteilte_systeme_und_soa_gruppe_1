package hsrt.VSundSOA.backendapi.gameInstance;

import hsrt.VSundSOA.backendapi.fieldCreation.FieldCreation;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import java.util.*;

/* unbenutzt da die Zeit nicht gereicht hat
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
*/

/**
 * The controller that will transmit and receive information concerning the development of the game
 */
@CrossOrigin(origins = "*")
@RestController
public class GameController {
    private ArrayList<Placement> history = new ArrayList<>();
    private Field field = new Field();

    /**
     * creates a new sudoku grid through the field creation module
     *
     * @return an array of int arrays (int[][]) that are used to save the sudoku grid
     */
    @PostConstruct
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/game/newField")
    public int[][] createField(){
        try {
            Thread.sleep(2000);
        }catch (Exception e){
            e.printStackTrace();
        }
        history.clear();

        //Leider wird mir die Zeit knapp und ich kann es nicht mehr über einem richtigem request machen. Aber so war mein Ansatz.
        /*
        StringBuffer content = new StringBuffer();
        try {
            URL url = new URL("localhost:8080/field/create");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            con.disconnect();
        }catch (Exception e){

        }
        System.out.println(content);
        */

        FieldCreation fieldCreation = new FieldCreation();
        ArrayList<int[][]> list = fieldCreation.getNewField();
        field.setStartField(list.get(0));
        field.setSolutionField(list.get(1));
        return field.getStartField();
    }

    /**
     * Returns the existing sudoku grid
     *
     * @return an array of int arrays (int[][]) that are used to save the sudoku grid
     */
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/game/field")
    public int[][] getField(){
        return field.getStartField();
    }

    /**
     * Asserts wether a placed number in the sudoku grid is correct
     *
     * @param placement Class containing: username of player, position X of placement, position Y of placement and placed number
     * @return true if the placed number is correct
     */
    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/game/placement")
    public Boolean ValidatePlacement(@Valid @RequestBody @NotNull Placement placement){
        if (field.getSolutionField()[placement.getX()][placement.getY()] == placement.getValue()){
            history.add(placement);
            return true;
        }
        return false;
    }

    /**
     * Asserts wether the game is over or not
     *
     * @return true if the game is won
     */
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/game/win")
    public Boolean verifyWin(){
        if (field.getCellsToFill() <= history.size()){
            return true;
        }
        return false;
    }


    /**
     * returns the log of every play made in this game
     *
     * @return An arraylist of placements containing every play made
     */
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/game/history")
    private ArrayList<Placement> getHistory(){
        return history;
    }
}
