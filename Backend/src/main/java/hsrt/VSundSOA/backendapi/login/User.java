package hsrt.VSundSOA.backendapi.login;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class User {
    private String username;
    private String color;
    private Integer gamesPlayed;
}
