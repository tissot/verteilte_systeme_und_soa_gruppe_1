package hsrt.VSundSOA.backendapi.login;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * The controller that will receive and manage the player information
 */
@CrossOrigin(origins = "*")
@RestController
public class LoginController {
    private ArrayList<User> userList = new ArrayList<>();

    /**
     * Function to get the whole list of players
     *
     * @return A list of users
     */
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/login/userList")
    public ArrayList<User> getUserList(){
        return userList;
    }

    /**
     * Function to get the information of one user
     *
     * @param username String in the request path that is used as key to find the user
     * @return Requested user containing: username, color and number of played games
     */
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/login/{username}")
    public User getUser(@PathVariable String username){
        for (User user : userList) {
            if (username.equals(user.getUsername())) {
                return user;
            }
        }
        return null;
    }

    /**
     * Add a new user to the list
     *
     * @param user information about new user as: username, color and played games
     * @return either the new user or if it already existed, the existing one
     */
    @ResponseStatus(HttpStatus.OK)
    @PostMapping("/login/registry")
    public User postUser(@RequestBody User user){
        for (User userInList : userList) {
            if (user.getUsername().equals(userInList.getUsername())) {
                return userInList;
            }
        }
        userList.add(user);
        return user;
    }

    /**
     * Change the saved data of a user (except the username)
     *
     * @param username String in the request path
     * @param user information to describe the changes to the user
     * @return the updated user
     */
    @ResponseStatus(HttpStatus.OK)
    @PatchMapping("/login/update/{username}")
    public User patchUser(@PathVariable String username, @RequestBody User user){
        User userToUpdate = getUser(username);
        userToUpdate.setColor(user.getColor());
        userToUpdate.setGamesPlayed(user.getGamesPlayed());
        return userToUpdate;
    }
}