const GameAPI = 'http://localhost:8080';

let username = '';
let activeNumber = 0;
let history = [];
let colors = ['gray', 'cyan', 'darkred', 'green', 'yellow', 'indianred', 'lime', 'orangered', 'lightblue', 'salmon', 'indigo', 'white', 'palegreen', 'teal', 'olive', 'deeppink'];
let users = {};
let reloadInterval;

async function initiateGameAsync() {
    users[username] = await postUserAsync(username, colors[Math.floor(Math.random() * colors.length)], 0);
    createEmptySudokuGrid();
    createUserList();

    await fillSudokuGridAsync();
    await reloadOnTimerAsync();

   reloadInterval = setInterval(reloadOnTimerAsync, 2 * 1000);
}

async function loginOnClickAsync() {
    username = document.getElementById('username').value;
    document.getElementById('login').remove();
    document.getElementById('username').remove();
    document.getElementById("usernameMessage").textContent = username;

    createNumberPadButtons();
    await initiateGameAsync();
}

function createNumberPadButtons() {
    const div = document.getElementById('number-pad');
    for (let i = 1; i <= 9; i++) {
        const input = document.createElement('input');
        input.className = 'PadButton';
        input.id = `number-pad-${i}`;
        input.type = 'button';
        input.value = i.toString();
        input.style.backgroundColor = 'Gray';
        input.onclick = () => numbersOnClick(i);
        div.append(input);
    }
}

function createEmptySudokuGrid() {
    const table = document.getElementById('Sudoku');
    for (let block = 0; block < 9; block++) {
        const blockDiv = document.createElement('div');
        blockDiv.className = 'block';
        table.appendChild(blockDiv)
        for (let row = 0; row < 3; row++) {
            for (let column = 0; column < 3; column++) {
                const cellDiv = document.createElement('div');
                cellDiv.id = `${row + (Math.floor(block / 3) * 3)}cell${column + ((block % 3) * 3)}`;
                cellDiv.className = `cell`;
                cellDiv.onclick = () => sudokuOnClickAsync(row + (Math.floor(block / 3) * 3), column + ((block % 3) * 3));
                blockDiv.appendChild(cellDiv);
            }
        }
    }
}

function createUserList() {
    const userBlock = document.getElementById(`userBlock`);
    const userList = document.getElementById(`userBlock`).children;
    for (const key of Object.keys(users)) {
        let occurrence = 0;
        for (const username of userList) {
            if (username.id == `user-${key}`) {
                occurrence++;
                break;
            }
        }
        if (occurrence == 0) {
            const user = document.createElement(`h2`);
            user.id = `user-${key}`
            user.textContent = `${key} won: ${users[key].gamesPlayed}`;
            user.style.color = users[key].color;

            userBlock.appendChild(user);
        }
    }
}

async function fillSudokuGridAsync() {
    const sudoku = await getFieldsAsync();
    for (let row = 0; row < 9; row++) {
        for (let cell = 0; cell < 9; cell++) {
            const cellText = document.createElement('h3');
            if (sudoku[row][cell] > 0) {
                cellText.innerText = sudoku[row][cell];
            } else {
                cellText.innerText = '-';
            }
            const currentCell = document.getElementById(`${row}cell${cell}`);
            currentCell.append(cellText);
        }
    }
}

async function createFieldAsync(){
    const fields = await fetch(GameAPI + "/game/newField", {method: "GET"});
    return await fields.json();
}

async function getFieldsAsync() {
    const field = await fetch(GameAPI + "/game/field", {method: "GET"});
    return await field.json();
}

async function getHistoryAsync() {
    const history = await fetch(GameAPI + "/game/history", {method: "GET"});
    return await history.json();
}

async function getWinVerificationAsync() {
    const win = await fetch(GameAPI + "/game/win", {method: 'GET'});
    return await win.json();
}

async function getUserAsync(username){
    const user = await fetch(GameAPI + `/login/${username}`, {method: 'GET'});
    return await user.json();
}

async function getUserListAsync(){
    const userList = await fetch(GameAPI + '/login/userList', {method: 'GET'});
    return await userList.json();
}

async function postUserAsync(username, color, gamesPlayed){
    const user = {
        username: username,
        color: color,
        gamesPlayed: gamesPlayed
    };
    const newUser = await fetch(GameAPI + `/login/registry`, {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(user)
    });
    return await newUser.json();
}

async function patchUserAsync(username, color, gamesPlayed){
    const user = {
        username: username,
        color: color,
        gamesPlayed: gamesPlayed
    };
    const updatedUser = await fetch(GameAPI + `/login/update/${username}`, {
        method: 'PATCH',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(user)
    });
    return await updatedUser.json();
}

async function reloadCellAsync(placement) {
    history.push(placement);
    let occurrence = 0;
    for (const key of Object.keys(users)) {
        if (key == placement.username) {
            occurrence++;
        }
    }
    if (occurrence == 0) {
        users[placement.username] = await getUserAsync(placement.username);
    }

    const cellText = document.getElementById(`${placement.x}cell${placement.y}`).children;
    cellText[0].innerText = placement.value;
    cellText[0].style.color = users[placement.username].color;

    if (await getWinVerificationAsync()) {
        clearInterval(reloadInterval);
        window.alert("you won!");
        const user = await getUserAsync(username);
        await patchUserAsync(username, user.color, ++user.gamesPlayed);
        // Reset
        if (placement.username === username){
            await createFieldAsync();
        }
        history = [];
        users = {};
        document.getElementById('Sudoku').remove();
        const field = document.createElement('div');
        field.id = 'Sudoku';
        field.className = 'Sudoku';
        document.getElementById('game').append(field);
        document.getElementById('userBlock').remove();
        const list = document.createElement('div');
        list.id = 'userBlock';
        list.className = 'UserList';
        document.getElementById('game').append(list);

        await initiateGameAsync();
    }
}

async function putPlacementAsync(placement) {
    const move = await fetch(GameAPI + "/game/placement", {
        method: "PUT",
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify(placement)
    });
    return await move.json();
}

async function reloadOnTimerAsync() {
    const historyList = await getHistoryAsync();
    for (const element of historyList) {
        if (history.find(h => h.x === element.x && h.y === element.y)) {
            continue;
        }

        await reloadCellAsync(element);
        createUserList();
    }
}

async function sudokuOnClickAsync(X, Y) {
    if (activeNumber < 1) {
        return;
    }
    const placement = {
        username: username,
        x: X,
        y: Y,
        value: activeNumber
    };
    const worked = await putPlacementAsync(placement);
    if (worked) {
        await reloadCellAsync(placement);
    }
}

function numbersOnClick(number) {
    activeNumber = number;
    for (let i = 1; i <= 9; i++) {
        document.getElementById(`number-pad-${i}`).style.backgroundColor = i === number ? 'blueviolet' : 'Gray';
    }
}